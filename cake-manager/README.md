# Cake Manager 

This is a Spring Boot application facilitates APIs to retrieve cake items or add new item to the system.

### Software and tools used to build the application

- `Java 8`
- `Maven 3.5.4`
- `Spring Boot 2.4.2`
- `macOs High Sierra`
- `IntelliJ IDEA`
- `Docker 3.1.0 (51484)`

### Running the application
Three ways to run the application

- Either run `mvn spring-boot:run` on command line at the directory `cake-manager`.
- OR, run `mvn package` first and then `java -jar target/cake-manager-0.0.1-SNAPSHOT.jar` at the directory `cake-manager`.
- OR, in intelliJ IDE right click `CakeManagerApplication.java` and choose run. 

### Rest APIs
The following APIs are available to access on any rest client, for example, `Postman` or `Advanced Rest Client` after running the application.

* To get cakes currently available in the system \
  HTTP Method: GET \
  URL: `http://localhost:8282/cakes`
  
* To add new cake entity \
  HTTP Method: POST \
  URL: `http://localhost:8282/cakes` \
  Request Body:
  `{
  "title":"A Fruit Cake",
  "desc": "A Fruit Cake Description",
  "image": "https://s3-eu-west-1.amazonaws.com/s3.mediafileserver.co.uk/carnation/WebFiles/RecipeImages/lemoncheesecake_lg.jpg"
  }`

### Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.2/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.2/maven-plugin/reference/html/#build-image)

