package com.waracle.cakemgr.service;

import com.waracle.cakemgr.model.Cake;

import java.util.List;

public interface CakeService {
    List<Cake> getCakes();
    Cake add(final Cake cake);
    List<Cake> addAll(final List<Cake> cakes);
}
