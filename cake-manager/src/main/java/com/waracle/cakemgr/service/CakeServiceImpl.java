package com.waracle.cakemgr.service;

import com.waracle.cakemgr.exception.InvalidResourceException;
import com.waracle.cakemgr.exception.ResourceAlreadyExists;
import com.waracle.cakemgr.model.Cake;
import com.waracle.cakemgr.repository.CakeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CakeServiceImpl implements CakeService {

    private final CakeRepository cakeRepository;
    private final ConversionService conversionService;

    @Override
    public List<Cake> getCakes() {
        return cakeRepository.findAll().stream()
                .map(cake -> conversionService.convert(cake, Cake.class))
                .collect(Collectors.toList());
    }

    @Override
    public Cake add(final Cake cake) {
        if (cake == null) {
            throw new InvalidResourceException("Invalid input provided");
        }
        final com.waracle.cakemgr.entity.Cake cakeEntity = save(cake);

        return conversionService.convert(cakeEntity, com.waracle.cakemgr.model.Cake.class);
    }

    @Override
    public List<Cake> addAll(final List<Cake> cakes) {
        if (cakes == null) {
            throw new InvalidResourceException("Invalid input provided");
        }
        final List<com.waracle.cakemgr.entity.Cake> cakeEntities = saveAll(cakes);

        return cakeEntities.stream()
                .map(cake -> conversionService.convert(cake, com.waracle.cakemgr.model.Cake.class))
                .collect(Collectors.toList());
    }

    private com.waracle.cakemgr.entity.Cake save(final Cake cake) {
        final com.waracle.cakemgr.entity.Cake cakeFound = cakeRepository.findCakeByTitle(cake.getTitle());

        if(cakeFound != null) {
            throw new ResourceAlreadyExists(
                    String.format("The cake entity (%s) is already exists.", cakeFound.getTitle()));
        }

        return cakeRepository.save(conversionService.convert(cake, com.waracle.cakemgr.entity.Cake.class));
    }

    private List<com.waracle.cakemgr.entity.Cake> saveAll(final List<Cake> cakes) {
        return cakeRepository.saveAll(getCakeEntities(cakes));
    }

    private List<com.waracle.cakemgr.entity.Cake> getCakeEntities(final List<Cake> cakes) {
        final Set<Cake> uniqueCakes = new LinkedHashSet<>(cakes);

        List<String> titles = uniqueCakes.stream().map(cake -> cake.getTitle()).collect(Collectors.toList());
        List<com.waracle.cakemgr.entity.Cake> cakeEntities = cakeRepository.findAllByTitleIn(titles);

        if(alreadyExists(cakeEntities)) {
            throw new ResourceAlreadyExists(
                    String.format("List of cakes provided contains the items (%s) that are already exist.",
                        titles.stream().collect(Collectors.joining(",")))
            );
        }

        return uniqueCakes.stream()
                .map(cake -> conversionService.convert(cake, com.waracle.cakemgr.entity.Cake.class))
                .collect(Collectors.toList());
    }

    private boolean alreadyExists(List<com.waracle.cakemgr.entity.Cake> cakeEntities) {
        return cakeEntities != null && cakeEntities.size() != 0;
    }
}
