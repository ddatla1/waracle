package com.waracle.cakemgr.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder(toBuilder = true)
@Table(name = "cake")
public class Cake {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @Column(name = "title",unique = true, nullable = false, length = 100)
    private String title;

    @Column(name = "desc", nullable = false, length = 100)
    private String desc;

    @Column(name = "image", nullable = false, length = 300)
    private String image;
}
