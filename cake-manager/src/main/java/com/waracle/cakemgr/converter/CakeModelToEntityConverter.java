package com.waracle.cakemgr.converter;

import com.waracle.cakemgr.model.Cake;
import org.springframework.core.convert.converter.Converter;

public class CakeModelToEntityConverter implements Converter<Cake, com.waracle.cakemgr.entity.Cake> {
    @Override
    public com.waracle.cakemgr.entity.Cake convert(Cake cake) {
        return com.waracle.cakemgr.entity.Cake.builder()
                .title(cake.getTitle())
                .desc(cake.getDesc())
                .image(cake.getImage())
                .build();
    }
}
