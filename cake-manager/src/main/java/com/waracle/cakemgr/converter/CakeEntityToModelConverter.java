package com.waracle.cakemgr.converter;

import com.waracle.cakemgr.entity.Cake;
import org.springframework.core.convert.converter.Converter;

public class CakeEntityToModelConverter implements Converter<Cake, com.waracle.cakemgr.model.Cake> {
    @Override
    public com.waracle.cakemgr.model.Cake convert(Cake cake) {
        return com.waracle.cakemgr.model.Cake.builder()
                .title(cake.getTitle())
                .desc(cake.getDesc())
                .image(cake.getImage())
                .build();
    }
}
