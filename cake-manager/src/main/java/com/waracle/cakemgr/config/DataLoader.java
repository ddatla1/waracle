package com.waracle.cakemgr.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.waracle.cakemgr.model.Cake;
import com.waracle.cakemgr.service.CakeService;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class DataLoader implements ApplicationListener<ApplicationReadyEvent> {

    @Value("${cake.manager.resource.url}")
    private String url;

    private final RestTemplate restTemplate;
    private final ObjectMapper objectMapper;
    private final CakeService cakeService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        final ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        try {
            final Cake[] cakes = objectMapper.readValue(response.getBody(), Cake[].class);

            if(cakes != null) {
                cakeService.addAll(Arrays.asList(cakes));
            }
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
