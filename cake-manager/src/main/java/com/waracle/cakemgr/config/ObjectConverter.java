package com.waracle.cakemgr.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.waracle.cakemgr.converter.CakeEntityToModelConverter;
import com.waracle.cakemgr.converter.CakeModelToEntityConverter;

@Configuration
public class ObjectConverter implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new CakeModelToEntityConverter());
        registry.addConverter(new CakeEntityToModelConverter());
    }
}