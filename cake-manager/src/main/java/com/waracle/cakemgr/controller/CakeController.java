package com.waracle.cakemgr.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.waracle.cakemgr.model.Cake;
import com.waracle.cakemgr.service.CakeService;

import javax.validation.Valid;

@RestController
@CrossOrigin
public class CakeController {

    private final CakeService cakeService;

    public CakeController(CakeService cakeService) {
        this.cakeService = cakeService;
    }

    @GetMapping("/cakes")
    ResponseEntity<List<Cake>> getCakes() {
        return new ResponseEntity<>(cakeService.getCakes(), HttpStatus.OK);
    }

    @PostMapping(value = "/cakes", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Cake> add(@Valid @RequestBody final Cake cake) {
        return new ResponseEntity<>(cakeService.add(cake), HttpStatus.OK);
    }
}
