package com.waracle.cakemgr.service;

import java.util.Arrays;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.ConversionService;

import com.waracle.cakemgr.exception.InvalidResourceException;
import com.waracle.cakemgr.exception.ResourceAlreadyExists;
import com.waracle.cakemgr.repository.CakeRepository;

import static com.waracle.cakemgr.helper.DataHelper.mockEntityData;
import static com.waracle.cakemgr.helper.DataHelper.mockModelData;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceImplTest {

    @Mock
    private CakeRepository cakeRepository;

    @Mock
    private ConversionService conversionService;

    private CakeServiceImpl cakeServiceImpl;

    @Before
    public void setup() {
        cakeServiceImpl = new CakeServiceImpl(cakeRepository, conversionService);
    }

    @Test
    public void getCakes_ShouldReturnListOfAvailableCakes() {
        // Arrange
        when(cakeRepository.findAll()).thenReturn(mockEntityData());
        when(conversionService.convert(any(), any())).thenReturn(mockModelData());

        // Act
        List<com.waracle.cakemgr.model.Cake> cakes = cakeServiceImpl.getCakes();

        // Assert
        verify(conversionService, times(1)).convert(any(), any());

        Assertions.assertThat(cakes.get(0).getTitle()).isEqualTo("Chocolate Cake");
        Assertions.assertThat(cakes.get(0).getDesc()).isEqualTo("Chocolate Cake Description");
        Assertions.assertThat(cakes.get(0).getImage()).isEqualTo("chocolate-cake.png");
    }

    @Test(expected = InvalidResourceException.class)
    public void add_WithInvalidParameter_ShouldReturnInvalidResourceException() {
        // Act
        cakeServiceImpl.add(null);
    }

    @Test(expected = InvalidResourceException.class)
    public void addAll_WithInvalidParameter_ShouldReturnInvalidResourceException() {
        // Act
        cakeServiceImpl.addAll(null);
    }

    @Test(expected = ResourceAlreadyExists.class)
    public void add_WithAlreadyExistingInput_ShouldReturnResourceAlreadyExistsException() {
        // Arrange
        when(cakeRepository.findCakeByTitle(any())).thenReturn(mockEntityData().get(0));

        // Act
        cakeServiceImpl.add(mockModelData());
    }

    @Test(expected = ResourceAlreadyExists.class)
    public void addAll_WithAlreadyExistingInput_ShouldReturnResourceAlreadyExistsException() {
        // Arrange
        when(cakeRepository.findAllByTitleIn(any())).thenReturn(mockEntityData());

        // Act
        cakeServiceImpl.addAll(Arrays.asList(mockModelData()));
    }
}

