package com.waracle.cakemgr.helper;

import java.util.Arrays;
import java.util.List;

import com.waracle.cakemgr.entity.Cake;

public class DataHelper {
    public static com.waracle.cakemgr.model.Cake mockModelData() {
        return com.waracle.cakemgr.model.Cake.builder()
                .title("Chocolate Cake")
                .desc("Chocolate Cake Description")
                .image("chocolate-cake.png")
                .build();
    }

    public static List<Cake> mockEntityData() {
        Cake aChocolateCake = Cake.builder()
                .title("Chocolate Cake")
                .desc("Chocolate Cake Description")
                .image("chocolate-cake.png")
                .build();

        return Arrays.asList(aChocolateCake);
    }
}
