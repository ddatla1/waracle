package com.waracle.cakemgr.controller;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.waracle.cakemgr.exception.ResourceAlreadyExists;
import com.waracle.cakemgr.service.CakeService;

import static com.waracle.cakemgr.helper.DataHelper.mockModelData;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CakeControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CakeService cakeService;

    @Test
    public void getCakes_ShouldReturnAvailableCakes() throws Exception {
        when(this.cakeService.getCakes()).thenReturn(Arrays.asList(mockModelData()));
        this.mockMvc.perform(get("/cakes"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].title").value("Chocolate Cake"))
                .andExpect(jsonPath("$[0].desc").value("Chocolate Cake Description"))
                .andExpect(jsonPath("$[0].image").value("chocolate-cake.png"));
    }

    @Test
    public void add_ShouldAddCakeToDatabase() throws Exception {
        when(this.cakeService.add(any())).thenReturn(mockModelData());
        this.mockMvc.perform(post("/cakes", mockModelData())
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(mockModelData())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("title").value("Chocolate Cake"))
                .andExpect(jsonPath("desc").value("Chocolate Cake Description"))
                .andExpect(jsonPath("image").value("chocolate-cake.png"));
    }

    @Test
    public void add_WithAlreadyExistingEntry_ShouldReturnConflictWith409StatusCode() throws Exception {
        when(this.cakeService.add(any())).thenThrow(
                new ResourceAlreadyExists("The cake entity (Chocolate Cake) is already exists."));

        this.mockMvc.perform(post("/cakes")
                .contentType("application/json")
                .content(objectMapper.writeValueAsString(mockModelData())))
                .andExpect(status().isConflict())
                .andExpect(jsonPath("code").value(HttpStatus.CONFLICT.value()))
                .andExpect(jsonPath("message")
                        .value("The cake entity (Chocolate Cake) is already exists."));
    }
}
