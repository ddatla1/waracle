package com.waracle.cakemgr.converter;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.waracle.cakemgr.model.Cake;

import static com.waracle.cakemgr.helper.DataHelper.mockEntityData;

public class CakeEntityToModelConverterTest {

    private CakeEntityToModelConverter converter;

    @Before
    public void setup() {
        converter = new CakeEntityToModelConverter();
    }

    @Test
    public void convert_ShouldConvertEntityToModel() {
        // Act
        Cake cake = converter.convert(mockEntityData().get(0));

        // Assert
        Assertions.assertThat(cake.getTitle()).isEqualTo("Chocolate Cake");
        Assertions.assertThat(cake.getDesc()).isEqualTo("Chocolate Cake Description");
        Assertions.assertThat(cake.getImage()).isEqualTo("chocolate-cake.png");
    }

}
