package com.waracle.cakemgr.converter;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import com.waracle.cakemgr.entity.Cake;

import static com.waracle.cakemgr.helper.DataHelper.mockModelData;

public class CakeModelToEntityConverterTest {
    private CakeModelToEntityConverter converter;

    @Before
    public void setup() {
        converter = new CakeModelToEntityConverter();
    }

    @Test
    public void convert_ShouldConvertModelToEntity() {
        // Act
        Cake cake = converter.convert(mockModelData());

        // Assert
        Assertions.assertThat(cake.getTitle()).isEqualTo("Chocolate Cake");
        Assertions.assertThat(cake.getDesc()).isEqualTo("Chocolate Cake Description");
        Assertions.assertThat(cake.getImage()).isEqualTo("chocolate-cake.png");
    }
}
