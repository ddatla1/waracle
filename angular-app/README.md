# Angular App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.4.

## To start up the application

Run `ng serve` to start the application. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

# After application start up
Click on the link `http://localhost:4200/` that will load the available cakes in the system currently.
There is a tab on the top banner `Add Cake` to add a new item.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
