import {Component, OnInit} from '@angular/core';
import {HttpService} from '../http/http.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  cake: any = {
    title: '',
    desc: '',
    image: ''
  };

  errorMessage: any = '';

  constructor(private http: HttpService, private router: Router) {
  }

  ngOnInit(): void {
  }

  addCake(): any {
    this.http.addCake(this.cake).subscribe(data => {
        console.log(data);
        this.router.navigate(['']).then();
      }, error => {
        this.errorMessage = error.error.message;
        console.log('error: ' + JSON.stringify(this.errorMessage));
      }
    );
    console.log('Data binding: ' + this.cake.title + ', ' + this.cake.desc + ', ' + this.cake.image);
  }
}
