import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http/http.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  cakes: any;

  constructor(private http: HttpService) { }

  ngOnInit(): void {
    this.http.getCakes().subscribe(data => {
        this.cakes = data;
        console.log(this.cakes);
      }
    );

    /*this.http.addCake().subscribe(data => {
        this.cakes = data;
        console.log(this.cakes);
      }
    );*/

  }

}
