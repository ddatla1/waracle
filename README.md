# Cake Manager

### Applications
This project has two separate applications

* cake-manager: This is a Spring Boot application facilitates Rest APIs to retrieve available cakes in the system currently, or to add new cake entity.
* angular-app: An Angular client application to show a list of available cakes when the application first start up, and also provides a tab `Add Cake` on the top banner to add a new cake entity. Also, clicking on the `Cake Manager` label on the top banner loads the landing page.

### Software and tools used to build server and client applications

- `Java 8`
- `Maven 3.5.4`
- `Spring Boot 2.4.2`
- `macOs High Sierra`
- `IntelliJ IDEA`
- `Docker 3.1.0 (51484)`
- `Angular CLI 10.1.4`

### How to run client and server applications at the same time
Provided the Docker is running, please follow following steps to run these applications.

* Open the Terminal in on Apple MacBook (or) Command Prompt in Windows.
* Then `cd` to the directory `waracle`
* Run `mvn -f cake-manager/pom.xml clean package`
* Run `docker-compose up --build`

After the Docker compose has successfully built, run the url `http://localhost:4200/` on the browser to access Angular client application.
At first a landing page will be presented to show up list of cakes currently available in the system.
There is also a tab on the top banner `Add Cake` to add new item.

Please also refer to the README.md files in the directories `angular-app` and `cake-manager` respectively for additional details.